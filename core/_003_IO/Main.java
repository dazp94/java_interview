package _003_IO;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {


		  public static void main(String[] args) {
			Main obj = new Main();
			String content = obj.getFile("resources/hello.txt");
			
			System.out.println("The content is: " + content);
			
			char toDelete = 'l';
			
			String deletedCharacters = obj.deleteCharacters(toDelete, content);
			System.out.println("Deleted " + toDelete + "\nThe new content is: " + deletedCharacters);
		  }

		  private String getFile(String fileName) {

			StringBuilder result = new StringBuilder("");

			//Get file from resources folder
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource(fileName).getFile());

			try (Scanner scanner = new Scanner(file)) {

				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					result.append(line).append("\n");
				}

				scanner.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
				
			return result.toString();

		  }
		  
		  private String deleteCharacters(char c, String content) {
			  
			 char[] toCharArray = content.toCharArray();
			 for (int i = 0; i < toCharArray.length; i++) {
				if(toCharArray[i]==c) {
					toCharArray[i] = ' ';
				}
			}
			String deletedChars = String.copyValueOf(toCharArray);
			deletedChars = deletedChars.replaceAll(" ", "");
			return deletedChars;
			
		  }
		}
