package _002_StaticNestedClass;

class TestOuter{  
    static int data=30;  
    
    private int numberOne = 1;
    
    static class Inner{  
     
    	void msg(){
    	 System.out.println("data is " + data);
    	 
    	 //This field can't be accessed because it isn't static
    	 //If you remove the comment from the following line you'll see an error
    	 //System.out.println("hello is " + numberOne);
    	 }
    	
    }
    
    public static void main(String args[]){  
    TestOuter.Inner obj=new TestOuter.Inner();  
    obj.msg();  
    }  
  }  