package _001_InnerClass;

/*
 * Inner class example:
 * 
 * As with instance methods and variables, an inner class is associated with an instance of its enclosing class and has direct access to that object's methods and fields.
 * Also, because an inner class is associated with an instance, it cannot define any static members itself.
 * Objects that are instances of an inner class exist within an instance of the outer class.
 * 
 * */
public class ShadowTest {

    public int x = 0;

    class FirstLevel {

        public int x = 1;

        void methodInFirstLevel(int x) {
            System.out.println("x = " + x);
            System.out.println("this.x = " + this.x);
            System.out.println("ShadowTest.this.x = " + ShadowTest.this.x);
            
            this.x = x;
            ShadowTest.this.x=x;
            
            System.out.println("this.x = " + this.x);
            System.out.println("ShadowTest.this.x = " + ShadowTest.this.x);
        }
    }

    public static void main(String... args) {
        ShadowTest st = new ShadowTest();
        ShadowTest.FirstLevel fl = st.new FirstLevel();
        fl.methodInFirstLevel(23);
    }
}
