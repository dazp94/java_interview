package _001_fibonacci;
/*
 * Write a simple Java program which will print Fibonacci series e.g. 1 1 2 3 5 8 13 .... up to a given number. 
 * Implement it recursively and cycle based
*/
public class Main {

	static int n1 = 0;
	static int n2 = 1;
	static int n3 = 0;
	static int i = 0;
	public static void main(String[] args) {
		
		//Using recursion
		fibonacciRecursion(5);
		
		//Using cycles
		fibonnaciCycles(5);
	}
	
	
	private static void fibonacciRecursion(int number){
			
		if(n3==1) {
			System.out.print("fibonacciRecursion: " + n3 + "\n");
		}
		if (number >= 0) {
			n3 = n1 + n2;
			n1 = n2;
			n2 = n3;
			System.out.print("fibonacciRecursion: " + n3 + "\n cycle:" + i);
			i++;
			fibonacciRecursion(number - 1);
		}
		  
	}

	private static int fibonnaciCycles(int cicles){
		int fibo1=1, fibo2=1, fibonacci=1;
		
		for (int i = 0; i < cicles; i++) {
			
			if(cicles == 1) {
				System.out.println("fibonnaciCycles: " + fibonacci + "\n cycle:" + i);
			}
			fibonacci = fibo1 + fibo2;
			fibo1 = fibo2;
			fibo2 = fibonacci;
			System.out.println("fibonnaciCycles: " + fibonacci + "\n cycle:" + i);
		}
		return fibonacci;
	}
}
