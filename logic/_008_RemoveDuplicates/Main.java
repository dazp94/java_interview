package _008_RemoveDuplicates;

import java.util.HashSet;
import java.util.Set;

/*
 * Remove all duplicate occurences
 * */
public class Main {

	public static void main(String[] args) {
		
		Object[] array = {'a','b',1,'a',1,'d'};
		System.out.println(removeDuplicates(array));
	}

	private static String removeDuplicates(Object[] array) {
		Set charSet = new HashSet();
		
		for (int i = 0; i < array.length; i++) {
			charSet.add(array[i]);
		}
		String result = charSet.toString();
		return result;
	}
	
}
