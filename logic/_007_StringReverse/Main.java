package _007_StringReverse;

/*This problem is similar to the String Palindrome problem we have discussed above.
 *If you can solve that problem you can solve this as well.
 *You can use indexOf() or substring() to reverse a String or alternatively,
 *convert the problem to reverse an array by operating on character array instead of String.
 * */

public class Main {

	public static void main(String[] args) {
		System.out.println(stringInverter("hello"));
	}

	private static char[] stringInverter(String string) {
		
		char[] originalOrder = string.toCharArray();
		char[] invertedOrder = new char[originalOrder.length];
		int j=0;
		
		for (int i = originalOrder.length - 1; i >= 0; i--) {
			invertedOrder[j]=originalOrder[i];
			j++;
		}
		
		return invertedOrder;
	}
	
	

}
