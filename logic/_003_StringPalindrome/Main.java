package _003_StringPalindrome;
/*
 * 
 * You need to write a simple Java program to check if a given String is palindrome or not.
 *  A Palindrome is a String which is equal to the reverse of itself e.g.
 *  "Bob" is a palindrome because of the reverse of "Bob" is also "Bob". 
 * 
 * */
public class Main {

	public static void main(String[] args) {
		System.out.println(palindrome("abbbba"));
		System.out.println(palindrome("abbbbc"));
	}
	
	public static boolean palindrome(String s) {
		  if (s.length() <= 1)
		    return true;
		  else
		    return (s.charAt(0) == s.charAt(s.length()-1)) &&
		            palindrome(s.substring(1,s.length()-1));
		}

}
