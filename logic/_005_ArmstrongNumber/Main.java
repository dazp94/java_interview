package _005_ArmstrongNumber;

import java.util.ArrayList;
import java.util.List;

/*A number is called an Armstrong number if it is equal to the cube of its each digit.
 *For example, 153 is an Armstrong number because 153 = 1+ 125+27 which is equal to 1^3+5^3+3^3.
 *You need to write a program to check if given number is Armstrong number or not. 
 * 
 * */

public class Main {

	public static void main(String[] args) {
		System.out.println(isArmstrongNumber(153));
	}

	private static boolean isArmstrongNumber(int i) {
		
		int cubicSum = 0;
		
		List<Integer> allNumbers = new ArrayList<Integer>();
		String howManyNumbers = String.valueOf(i);
		
		for (int j = 0; j < howManyNumbers.length(); j++) {
			int newNumber = Integer.parseInt(howManyNumbers.charAt(j)+"");
			allNumbers.add(newNumber);
		}
		
		for (int j = 0; j < allNumbers.size(); j++) {
			cubicSum = cubicSum + cubicValue(allNumbers.get(j));
			System.out.println("cubicSum: " + cubicSum);
		}
		
		if(i == cubicSum) {
			return true;
		}
		return false;
	}
	
	private static int cubicValue(int i) {
		return i*i*i;
	}

}
