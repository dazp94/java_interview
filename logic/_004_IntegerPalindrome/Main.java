package _004_IntegerPalindrome;


/*This is generally asked as follow-up or alternative of the previous program.
 *This time you need to check if given Integer is palindrome or not.
 *An integer is called palindrome if its equal to its reverse e.g. 1001 is a palindrome but 1234 is not because the reverse of 1234 is 4321 which is not equal to 1234.
 *You can use divide by 10 to reduce the number and modulus 10 to get the last digit.
 *This trick is used to solve this problem.
 */
public class Main {


	public static void main(String[] args) {
		
		System.out.println(isIntegerPalindrome(67871));
		
		System.out.println(isIntegerPalindrome(67876));
		
		System.out.println(isIntegerPalindrome2(67871));
		
		System.out.println(isIntegerPalindrome2(67876));
	}

	private static boolean isIntegerPalindrome(int i) {
		String s = String.valueOf(i);
		System.out.println("Value: " + s);

		if(s.length() == 1 || s.length() == 0) {
			return true;
		}

		if(s.length() == 2) {
			if (s.charAt(0)==s.charAt(1)){
				System.out.println("i: " + i);
				return true;
			}
			else return false;
		}

		else {
			if(s.charAt(0) != s.charAt(s.length() - 1)) {
				System.out.println();
				System.out.println(s.charAt(0) + " != " +s.charAt(s.length() - 1));
				return false;
			}
			else {
				String newString = s.substring(1, s.length()-1);
				int newInt = Integer.parseInt(newString);
				isIntegerPalindrome(newInt);
			}
			return true;
		}
	}
	
	private static boolean isIntegerPalindrome2(int number) {
        int palindrome = number; // copied number into variable
        int reverse = 0;

        while (palindrome != 0) {
            int remainder = palindrome % 10;
            reverse = reverse * 10 + remainder;
            palindrome = palindrome / 10;
        }

        // if original and reverse of number is equal means
        // number is palindrome in Java
        if (number == reverse) {
            return true;
        }
        return false;
    }

}
