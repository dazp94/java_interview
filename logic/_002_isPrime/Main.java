package _002_isPrime;

/*Write a Java program to check if a given number is prime or not.
 *Remember, a prime number is a number which is not divisible by any other number
 *e.g. 3, 5, 7, 11, 13, 17 etc.

 * 
 * */

public class Main {

	public static void main(String[] args) {
		System.out.println(isPrime(1));
	}

	public static boolean isPrime(int n) {
		    
		//check if n is a multiple of 2
	    if (n%2==0) return false;
	    
	    //if not, then just check the odds
	    for(int i=3;i*i<=n;i+=2) {
	        if(n%i==0)
	            return false;
	    }
	    return true;
	}

}


