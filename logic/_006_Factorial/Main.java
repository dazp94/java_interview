package _006_Factorial;
/*This is one of the simplest programs you can expect on interviews.
 * It is generally asked to see if you can code or not.
 * Sometimes interviewer may also ask about changing a recursive solution to iterative one or vice-versa.
 * 
 * */
public class Main {

	public static void main(String[] args) {
		
		System.out.println("With cycles: " + factorialOf(10));
		System.out.println("With recursion: " + factorialRecursion(10));
	
	}

	private static int factorialOf(int i) {
		int factorial = i;
		
		while(i>=2) {
			factorial = factorial * (i - 1);
			i--;
		}
		
		return factorial;
	}
	
	private static int factorialRecursion(int number){       
        if(number == 0){
            return 1;
        }
        return number*factorialRecursion(number-1);
    }

}
